const Product = require("../models/Product");

module.exports.addProduct = (reqBody) => {
	console.log(reqBody);

	// create a new object
	let newProduct = new Product({
		name: reqBody.product.name,
		description: reqBody.product.description,
		price: reqBody.product.price
	});

	// saves the created object to our database
	return newProduct.save().then((product, error) => {
		// product creation failed
		if(error) {
			return false;
		}else {
			// product creation successful
			return true
		}
	})
}


// retrieving all products
module.exports.getAllProducts = () => {
	return Product.find({}).select('-buyers').then( result => {
		return result;
	})
}

// retrieve all active products
module.exports.getAllActive = () => {
	return Product.find({ isActive: true}).select('-buyers').then(result => {
		return result;
	})
}

// retrieve specific product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams).select('-buyers').then(result => {
		return result;
	})
}


module.exports.updateProduct = (productId, reqBody) => {
	// specify the properties of the doc to be updated
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	// findByIdAndUpdate(id, updatesToBeApplied)
	return Product.findByIdAndUpdate(productId, updatedProduct).then((product, error) => {
		// product not updated
		if(error) {
			return false;
		}else {
			// course updated successfully
			return true
		}
	})
}


// archive a product
module.exports.archiveProduct = (reqParams) => {
	let updateActiveField = {
		isActive: false
	}
	return Product.findByIdAndUpdate(reqParams, updateActiveField).then((product, error) => {
		if(error){
			return false;
		} else {
			return `Product archived successfully.`;
		}
	})
}

module.exports.unarchiveProducts = (product, prodObj) => {
    let newStatus = {
        isActive : true
    }

        let id = product.productId
    return Product.findByIdAndUpdate(id, newStatus).then((result, err) =>{
        if (result) {
            return true
        } else {
            return false
        }
    })
};


