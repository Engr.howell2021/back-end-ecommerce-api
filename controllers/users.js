const User = require("../models/User");

const Product = require("../models/Product");

// encrypted password
const bcrypt = require('bcrypt');

const auth = require('../auth');

module.exports.checkEmailExists = (reqBody) => {
	return User.find({ email: reqBody.email }).then(result => {
		if(result.length > 0){
			return true
		} else {
			return false
		}
	})
}


// User Registration
/*
1. create a new User object
2. make sure that the password is encrypted
3. save the new User to db
*/

/*
Function parameters from routes to controllers

Routes (argument)
*/
module.exports.registerUser = (reqBody) => {

	// creates a new User Object
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		// mobileNo: reqBody.mobileNo,
		email: reqBody.email,
		// password: reqBody.password
		// 10 is the value provided as the number of 'salt' round that the bcrypt algo will run in order to encrypt the password
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	// saves the created object to db
	return newUser.save().then((user, error) => {
		if(error) {
			return false;
		} else {
			// user registration is successful
			return true
		}
	})

}

// retrieving users
module.exports.getUsers = () => {
	return User.find({}).then( result => {
		return result;
	})
}


module.exports.loginUser = (reqBody) => {
	return User.findOne({ email: reqBody.email }).then(result => {
		// user does not exist
		if(result == null){
			return `Email/password is incorrect`;
		} else {
			// user exists
			// the 'compareSync' method is used to compare a non-encrypted password from the login form to the encrypted password retrieved from the databse and returns 'true' or 'false'
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			// if the password match
			if(isPasswordCorrect){
				// generate an access token
				return { accessToken : auth.createAccessToken(result.toObject())}
			} else {
				// passord does not match
				return false
			}
		}
	})
}



// change user to admin
module.exports.changeToAdmin = (reqParams) => {
	let updateAdminField = {
		isAdmin: true
	}
	return User.findByIdAndUpdate(reqParams, updateAdminField).then((user, error) => {
		if(error){
			return false;
		} else {
			return true
		}
	})
}


// change user(admin) back to user
module.exports.changeToUser = (reqParams) => {
	let updateAdminField = {
		isAdmin: false
	}
	return User.findByIdAndUpdate(reqParams, updateAdminField).then((user, error) => {
		if(error){
			return false;
		} else {
			return true
		}
	})
}


// user places an order
module.exports.order = async (data) => {
	//Add the productId to the orders array of the user

	let isUserUpdated = await User.findById(data.userId).then( user => {
		//push the product Id to orders property
		user.orders.push({ productId: data.productId});

		//save
		return user.save().then((user, error) => {
			if(error) {
				return false;
			}else {
				return true
			}
		})
	});

	let isProductUpdated = await Product.findById(data.productId).then(product => {
		// add the userId in the product's database(buyers)
		product.buyers.push({ userId: data.userId });

		return product.save().then((product, error) => {
			if(error) {
				return false;
			}else {
				return true;
			}
		})
	});

	// Validation
	if(isUserUpdated && isProductUpdated){
		//user order successful
		return true;
	}else {
		//user order failed
		return false;
	}
}


// get all orders (admin role)
module.exports.getAllOrders = () => {
	return User.find({}).then((result) => {
		// console.log(result);
		let orders = [];

		result.forEach(user => {
			orders = [...orders, ...user.orders]
		})
		// console.log(orders)
		return orders;
	});
}




// retrieve authenticated user's orders
module.exports.getProfile = (data) => {
	return User.findById(data).then(result => {
		return result.orders;
	})
}
