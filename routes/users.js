const express = require("express");
const route = express.Router();
const auth = require("../auth");

const controller = require("../controllers/users");

route.post("/checkEmail", (req, res) => {
	controller.checkEmailExists(req.body).then(result => res.send(result))
});


// Registration for user
// http://localhost:4000/api/users/register
route.post("/register", (req, res) => {
	controller.registerUser(req.body).then(result => res.send(result));
})


// retrieve all users | for checking only
route.get("/all", (req, res) => {
	controller.getUsers().then(result => res.send(result));
})


// User Authentication(login)
route.post("/login", (req, res) => {
	controller.loginUser(req.body).then(result => res.send(result));
});


// change user to admin
route.put("/:userId/changeToAdmin", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		controller.changeToAdmin(req.params.userId).then(result => res.send(result))
	} else {
		res.send(`Sorry. Only admins can change user roles.`);
	}

})


// change user(admin) back to user
route.put("/:userId/changeToUser", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		controller.changeToUser(req.params.userId).then(result => res.send(result))
	} else {
		res.send(`Sorry. Only admins can change user roles.`);
	}

})


// checkout an order (user)
route.post("/checkout", auth.verify, (req, res) => {
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		productId : req.body.productId
	}

	controller.order(data).then(result => res.send(result));
})


// retrieve all orders from admin
route.get("/all", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	if(isAdmin) {
		UserController.getAllOrders().then(result => res.send(result));
	} else {
		res.send({ auth: "Order retrieval failed. Only admins can retrieve all orders." })
	}
});


// retrieve authenticated user's orders
//The "auth.verify" acts as a middleware to ensure that the user is logged in before they can get the details of a user
route.get("/myorders", auth.verify, (req, res) => {
	//decode() to retrieve the user information from the token passing the "token" from te request headers as an argument

	const userData = auth.decode(req.headers.authorization);

	controller.getProfile(userData.id).then(result => res.send(result))
} )



module.exports = route;
