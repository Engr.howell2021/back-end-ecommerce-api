const express = require("express");
const route = express.Router();
const controller = require("../controllers/products");

const auth = require("../auth");

// creating/adding a product
route.post("/create", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	
	if(data.isAdmin) {
		controller.addProduct(data).then(result => res.send(result));
	} else {
		res.send({ auth: "Product submission failed. Only admins can add products"})
	}
})


// retrieving all products
route.get("/all", (req, res) => {
	controller.getAllProducts().then(result => res.send(result));
})


// retrieving all active products
route.get("/active", (req, res) => {
	controller.getAllActive().then(result => res.send(result))
});


// retrieving a specific product
route.get("/:productId", (req, res) => {
	console.log(req.params.productId);
	controller.getProduct(req.params.productId).then(result => res.send(result))
})


//1 update a product
route.put("/:productId", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin){
		controller.updateProduct(req.params.productId, req.body).then(result => res.send(result));		
	} else {
		res.send(false);
	}
})


// archive a product
route.put("/:productId/archive", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		controller.archiveProduct(req.params.productId).then(result => res.send(result))
	} else {
		res.send(false);
	}

})

//[SECTION]FUNCTION UNArchive Product [ADMIN]
route.put('/unarchive/:productId', auth.verify,(req,res) =>{
    let isAdmin = auth.decode(req.headers.authorization).isAdmin
    isAdmin ? controller.unarchiveProducts(req.params, req.body).then(result => {
        res.send(result)
    })
    :res.send ('Unauthorized access')
})

// // Product Delete by Admin Only [ADMIN]
route.delete('/:id', auth.verify, (req,res) => {
    let id = req.params.id; 
    let isAdmin = auth.decode(req.headers.authorization).isAdmin
    isAdmin ? controller.deleteProduct(id).then(outcome => {
        res.send(outcome);
     }):
     res.send ('Unauthorized')
})



module.exports = route;



