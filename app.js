// [Section] Packages and Dependencies
const express = require("express");
const mongoose = require("mongoose");
const dotenv = require("dotenv");  
const productsRoutes = require('./routes/products');
const cors = require('cors')
// acquire the routing components for the users collection
const userRoutes = require('./routes/users')
// const orderRoutes = require('./routes/orders')



// [Section] Server Setup
const app = express();
dotenv.config();
app.use(cors())
app.use(express.json());
const secret = process.env.CONNECTION_STRING
const port = process.env.PORT

//[SECTION] Application Routes
app.use('/products', productsRoutes); 
app.use('/users', userRoutes)

// [Section] Database Connect
mongoose.connect(secret)
let dbStatus = mongoose.connection; 
dbStatus.on('open', () => console.log("Database is Connected")); 


// [Section] Gateway Response
app.get('/', (req, res) => {
    res.send('Welcome to my Ecommerce Capstone')
});
app.listen(port, () => console.log(`Server is running on port ${port}`))